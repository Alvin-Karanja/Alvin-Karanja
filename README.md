<h1 align="center">Hi 👋, I'm Alvin Karanja</h1>
<h3 align="center">A passionate Software Developer</h3>

<p align="left"> <img src="https://komarev.com/ghpvc/?username=alvin-karanja&label=Profile%20views&color=0e75b6&style=flat" alt="alvin-karanja" /> </p>

- 🌱 I’m currently learning **Kotlin, Jetpack Compose and Kotlin Multiplatform**

</p>
